# CIExample --
#
# This package is a simple example of integrating tcl and tcltest with a
# continuous integration test runner.
#

package provide CIExample 1.0.0
package require Tcl 8.5

namespace eval CIExample {
    # ::CIExample::add
    #
    # Add value `x` to value `y`
    #
    # Arguments:
    #   x {required} An integer
    #   y {required} An integer
    # Results:
    #   Returns the sum of `x` and `y`
    #
    proc add {x y} {
        if {![string is integer $x]} {
            error "Expected x to be an integer."
        }

        if {![string is integer $y]} {
            error "Expected y to be an integer."
        }

        return [expr $x + $y]
    }

    # ::CIExample::subtract
    #
    # Subtract value `x` from value `y`
    #
    # Arguments:
    #   x {required} An integer
    #   y {required} An integer
    # Results:
    #   Returns the difference between `x` and `y`
    #
    proc subtract {x y} {
        if {![string is integer $x]} {
            error "Expected x to be an integer."
        }

        if {![string is integer $y]} {
            error "Expected y to be an integer."
        }

        return [expr $y - $x]
    }

    # ::CIExample::multiply
    #
    # Multiply value `x` by value `y`
    #
    # Arguments:
    #   x {required} An integer
    #   y {required} An integer
    # Results:
    #   Returns the product of `x` and `y`
    #
    proc multiply {x y} {
        if {![string is integer $x]} {
            error "Expected x to be an integer."
        }

        if {![string is integer $y]} {
            error "Expected y to be an integer."
        }

        return [expr $x * $y]
    }
}
